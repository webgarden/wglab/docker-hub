#!/usr/bin/env bash

PHP=webgarden/php

build() {
	VER=$1

	shift

	docker build php/$VER/. --tag $PHP:$VER $*

	docker push webgarden/php
}

docker login -u $DOCKER_HUB_LOGIN -p $DOCKER_HUB_PASSWORD

build 5.6
build 7.0
build 7.1 --tag $PHP:latest
build 7.2 --tag $PHP:dev

docker logout
